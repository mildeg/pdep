/*Punto 1*/
creeEn(gabriel,campanita).
creeEn(gabriel,magoDeOz).
creeEn(gabriel,cavenaghi).

creeEn(juan,conejoPascua).

creeEn(macarena,reyesMago).
creeEn(macarena,magoCapria).
creeEn(macarena,campanita).



suenaCon(gabriel,loteria([5,9])).
suenaCon(gabriel,futbolista(arsenal)).
suenaCon(juan,cantante(100000)).
suenaCon(macarena,cantante(10000)).

equipoChico(arsenal).
equipoChico(aldosivi).

dificultad(cantante(Discos),6):- Discos > 500000.
dificultad(cantante(Discos),4):- Discos =< 500000. 
dificultad(loteria(Numeros),Puntaje):- length(Numeros,CantidadNumeros),Puntaje is CantidadNumeros*10.
dificultad(futbolista(Equipo),3):- equipoChico(Equipo). 
dificultad(futbolista(Equipo),16):- not(equipoChico(Equipo)).


ambiciosa(Persona):-suenaCon(Persona,_), findall(Puntitos,(suenaCon(Persona,Sueno), dificultad(Sueno,Puntitos)),Puntaje),sumlist(Puntaje,Suma), Suma > 20.

suenoMenor5(Persona):-suenaCon(Persona,Sueno),dificultad(Sueno,Valor),Valor < 5. 
suenosPuros(Persona):- suenaCon(Persona,_), forall(suenaCon(Persona,Sueno), suenoPuro(Sueno)).

suenoPuro(futbolista(_)).
suenoPuro(cantante(Discos)):- Discos < 200000.


quimica(campanita,Persona):-creeEn(Persona,campanita), suenoMenor5(Persona).
quimica(Personaje,Persona):- creeEn(Persona,Personaje) ,suenosPuros(Persona), not(ambiciosa(Persona)),Personaje \= campanita.

amigos(campanita,reyesMagos).
amigos(campanita,conejoPascua).
amigos(conejoPascua,cavenaghi).
amigosPorHerencia(Persona,Persona3):-amigos(Persona,Persona3).
amigosPorHerencia(Persona,Persona3):-amigos(Persona,Persona2) , amigosPorHerencia(Persona2,Persona3).
estaEnfermo(campanita).
estaEnfermo(reyesMagos).
estaEnfermo(conejoPascua).

puedeAlegrar(Personaje,Persona):- suenaCon(Persona,_),quimica(Personaje,Persona), not(estaEnfermo(Personaje)).
puedeAlegrar(Personaje,Persona):- suenaCon(Persona,_),quimica(Personaje,Persona), amigosPorHerencia(Personaje,PersonajePosible) ,not(estaEnfermo(PersonajePosible)),not(estaEnfermo(Personaje)).

puedeAlegrar2(Persona,Personaje):-suenaCon(Persona,_),quimica(Personaje,Persona), not(estaEnfermo(Personaje)).
puedeAlegrar2(Persona,Personaje):-suenaCon(Persona,_),quimica(Personaje,Persona), tieneBackup(Personaje),not(estaEnfermo(Personaje)).

tieneBackup(Personaje):-amigos(Personaje,Amigo),not(estaEnfermo(Amigo)).
tieneBackup(Personaje):-amigos(Personaje,Otro), tieneBackup(Otro).
