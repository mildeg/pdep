programa(isabel,cobol).
programa(isabel,visualBasic).
programa(isabel,java).

programa(julieta,java).
programa(marcos,java).
/*Nadie programó en Assembler. => No agregamos nada, solo se modela informacion verdadera.
Aún no sabemos si Julieta programó en Go.  => No agregamos nada , solo se modela informacion que sabemos que es cierta.
*/
programa(santiago,ecmascript).
programa(santiago,java).

rol(isabel,analistaFuncional).

rol(andres,projectLeader).
rol(Persona,programador):- esProgramador(Persona).

esProgramador(Persona) :- programa(Persona,_).

/*
2) programa(isabel, Lenguaje).
 Lenguaje = cobol ;
 Lenguaje = visualBasic ;
 Lenguaje = java.


 programa( Persona,java).
 Persona = isabel ;
 Persona = julieta ;
 Persona = marcos ;
 Persona = santiago.

programa(Persona,_).
false.

rol(isabel,programador).
true 

?- rol(isabel,Que).
Que = analistaFuncional ;
Que = programador ;
Que = programador ;
Que = programador.

 rol(Quien,programador).
Quien = isabel ;
Quien = isabel ;
Quien = isabel ;
Quien = julieta ;
Quien = marcos ;
Quien = santiago ;
Quien = santiago.

?- rol(_,projectLeader).
true.

*/

proyecto(sumatra,java).
proyecto(sumatra,net).
proyecto(prometeus,cobol).

trabajaEn(isabel,prometeus).
trabajaEn(santiago,prometeus).
trabajaEn(julieta,sumatra).
trabajaEn(marcos,sumatra).
trabajaEn(andres,sumatra).

/*En el predicado bienAsignada2 pusieron trabajaEn(Persona,Proyecto) en cada cláusula, y como no nos gusta mucho la lógica repetida,
 les conviene tener un predicado auxiliar para verificar que una persona corresponda bien al proyecto.*/

 
bienAsignada(Persona,Proyecto):- trabajaEn(Persona,Proyecto) , cumpleCondicion(Persona,Proyecto).
cumpleCondicion(Persona,Proyecto):- proyecto(Proyecto,Lenguaje),programa(Persona,Lenguaje).

cumpleCondicion(Persona,_):- rol(Persona,projectLeader).

cumpleCondicion(Persona,_):- rol(Persona,analistaFuncional).


