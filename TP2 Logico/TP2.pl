programa(isabel,cobol).
programa(isabel,visualBasic).
programa(isabel,java).

programa(julieta,java).
programa(marcos,java).
/*Nadie programó en Assembler. => No agregamos nada, solo se modela informacion verdadera.
Aún no sabemos si Julieta programó en Go.  => No agregamos nada , solo se modela informacion que sabemos que es cierta.
*/
programa(santiago,ecmascript).
programa(santiago,java).

rol(isabel,analistaFuncional).

rol(andres,projectLeader).
rol(Persona,programador):- esProgramador(Persona).

esProgramador(Persona) :- programa(Persona,_).

/*
2) programa(isabel, Lenguaje).
 Lenguaje = cobol ;
 Lenguaje = visualBasic ;
 Lenguaje = java.


 programa( Persona,java).
 Persona = isabel ;
 Persona = julieta ;
 Persona = marcos ;
 Persona = santiago.

programa(Persona,_).
false.

rol(isabel,programador).
true 

?- rol(isabel,Que).
Que = analistaFuncional ;
Que = programador ;
Que = programador ;
Que = programador.

 rol(Quien,programador).
Quien = isabel ;
Quien = isabel ;
Quien = isabel ;
Quien = julieta ;
Quien = marcos ;
Quien = santiago ;
Quien = santiago.

?- rol(_,projectLeader).
true.

*/

proyecto(sumatra,java).
proyecto(sumatra,net).
proyecto(prometeus,cobol).

trabajaEn(isabel,prometeus).
trabajaEn(santiago,prometeus).
trabajaEn(julieta,sumatra).
trabajaEn(marcos,sumatra).
trabajaEn(andres,sumatra).

/*En el predicado bienAsignada2 pusieron trabajaEn(Persona,Proyecto) en cada cláusula, y como no nos gusta mucho la lógica repetida,
 les conviene tener un predicado auxiliar para verificar que una persona corresponda bien al proyecto.*/

 
bienAsignada(Persona,Proyecto):- trabajaEn(Persona,Proyecto) , cumpleCondicion(Persona,Proyecto).
cumpleCondicion(Persona,Proyecto):- proyecto(Proyecto,Lenguaje),programa(Persona,Lenguaje).

cumpleCondicion(Persona,_):- rol(Persona,projectLeader).

cumpleCondicion(Persona,_):- rol(Persona,analistaFuncional).




/* Segunda Parte */


projectLeaders(Proyecto,Personas):- proyecto(Proyecto,_) ,findall(Persona,( trabajaEn(Persona,Proyecto),rol(Persona,projectLeader) ),Personas).
bienDefinido(Proyecto):- proyecto(Proyecto,_), forall(trabajaEn(Persona,Proyecto),bienAsignada(Persona,Proyecto)), projectLeaders(Proyecto,Leaders) , length(Leaders,1) .


esCopado(isabel,santiago).
esCopado(santiago,julieta).
esCopado(santiago,marcos).
esCopado(julieta,andres).
esCopadoPorHerencia(Persona1,Persona2):- esCopado(Persona1,Persona2).
esCopadoPorHerencia(Persona1,Persona2):- esCopado(Persona1,Alguien),esCopadoPorHerencia(Alguien,Persona2).

sabeYnosabe(Persona,OtraPersona,Lenguaje):-rol(OtraPersona,_),programa(Persona,Lenguaje),not(programa(OtraPersona,Lenguaje)).

/*puedeEnsenar(Persona,OtraPersona,Lenguaje):-sabeYnosabe(Persona,OtraPersona,Lenguaje) , esCopado(Persona,OtraPersona). La segunda solucion ya contempla a esta. */
puedeEnsenar(Persona,OtraPersona,Lenguaje):- sabeYnosabe(Persona,OtraPersona,Lenguaje) , esCopadoPorHerencia(Persona,OtraPersona).



tarea(isabel , evolutiva(compleja)). 
tarea(isabel, correctiva(8, brainfuck)).
tarea(isabel, algoritmica(150)).
tarea(marcos, algoritmica(20)).
tarea(julieta, correctiva(412, cobol)).
tarea(julieta, correctiva(21, go)).
tarea(julieta, evolutiva(simple)).




puntaje(evolutiva(simple),3).
puntaje(evolutiva(compleja),5).
puntaje(correctiva(CantidadLineas,_),4):- tarea(_,correctiva(CantidadLineas,_)),  CantidadLineas > 50.
puntaje(correctiva(_,brainfuck),4).
puntaje(algoritmica(CantidadLineas),Puntaje):- tarea(_,algoritmica(CantidadLineas)), Puntaje is CantidadLineas / 10.


seniority(Persona,Puntaje) :- rol(Persona,_),findall( Puntos ,( tarea(Persona,Tarea),puntaje(Tarea,Puntos) ),Puntitos),sumlist(Puntitos,Puntaje).




























